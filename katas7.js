//======================================================
//               Katas 7 by Alec Stephens
//======================================================

//Global Test Variables

let arrayOfLetters = ["f", "g", "y","w"]
let arrayOfNumbers = [2,3,4,5,6,7]

//======================================================
//                      ForEach
//======================================================
function newForEach(array, callback){
    for (let index= 0 ; index < array.length; index++){
        let value = array[index]
        callback(value, index, array)
    }
}
//======================================================
//                        Map
//======================================================

function newMap (array, callback ){
    let newValue = [];

    for(let index = 0; index <array.length; index++){
        newValue[index] = callback(array[index], index)
    }
    return newValue
}

//======================================================
//                       Filter
//======================================================

function newFilter (array, callback){
    let newValue = [];

    for(let index = 0; index <array.length; index++){
        if ((callback(array[index], index))){
            newValue.push(array[index])
        }
    }
    return newValue
}

//======================================================
//                         Some
//======================================================

function newSome (array, callback){
    for(let index = 0; index <array.length; index++){
        if ((callback(array[index], index))){
            return true
        }
        else{
            return false
        }
    }
}

//======================================================
//                         Find
//======================================================

function newFind(array, callback){
    let newValue = [];

    for(let index = 0; index <array.length; index++){
        if ((callback(array[index], index))){
            newValue.push(array[index])
            break
        }
    }
    return newValue
}

//======================================================
//                     FindIndex
//======================================================

function newFindIndex(array, callback){
    for(let index = 0; index <array.length; index++){
        if ((callback(array[index], index))){
            return index
            break
        }
    }
}

//======================================================
//                        Every
//======================================================

function newEvery(array, callback){
    let isTrue = true
    for(let index = 0; index <array.length; index++){
        if (callback(array[index]) === false){
            isTrue = false
        }
    }
    return isTrue
}

//Console Log Tests

console.log(newFilter(arrayOfNumbers, arrayOfNumbers => arrayOfNumbers > 5))
console.log(newMap(arrayOfNumbers, arrayOfNumbers => ++arrayOfNumbers))
console.log(newSome(arrayOfNumbers, arrayOfNumbers => arrayOfNumbers <= 2))
console.log(newFind(arrayOfNumbers, arrayOfNumbers => arrayOfNumbers > 5))
console.log(newFindIndex(arrayOfNumbers, arrayOfNumbers => arrayOfNumbers > 5))
console.log(newEvery(arrayOfNumbers, arrayOfNumbers => arrayOfNumbers >= 1))